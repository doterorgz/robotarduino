/*
  Ping))) Sensor

  This sketch reads a PING))) ultrasonic rangefinder and returns the distance
  to the closest object in range. To do this, it sends a pulse to the sensor to
  initiate a reading, then listens for a pulse to return. The length of the
  returning pulse is proportional to the distance of the object from the sensor.

  The circuit:
	- +V connection of the PING))) attached to +5V
	- GND connection of the PING))) attached to ground
	- SIG connection of the PING))) attached to digital pin 7

  created 3 Nov 2008
  by David A. Mellis
  modified 30 Aug 2011
  by Tom Igoe

  This example code is in the public domain.

  http://www.arduino.cc/en/Tutorial/Ping
*/

// this constant won't change. It's the pin number of the sensor's output:
const int pingPins[4] = {2,3,4,5};//
const int echoPins[4] = {8,9,10,11};//
const int leds[4] = {7,6,12,13};


long menorDistancia = 100000;
short ledMenorDistancia = -1;
void setup() {
  // initialize serial communication:
  Serial.begin(9600);
  pinMode(6,OUTPUT);
  pinMode(7,OUTPUT);
  pinMode(12,OUTPUT);
  pinMode(13,OUTPUT);
}

void loop() {
  long durations[4];   //, durationCR, durationCL, durationLL;
  long distancias[4];    //RR, distanciaCR, distanciaCL, distanciaLL;
  menorDistancia = 100000;
  for (short i=0;i<4;i++)  {
    durations[i] = calculaTiempo(pingPins[i],echoPins[i]);
    
    distancias[i]=microsecondsToCentimeters(durations[i]);
    if (distancias[i]<menorDistancia) {
      menorDistancia=distancias[i];
      ledMenorDistancia=i;
    }
    Serial.print(durations[i]);
    Serial.print("ms->");
    Serial.print(distancias[i]);
    Serial.print(" cm, -- ");
  }
  encenderLed(ledMenorDistancia);
  Serial.println();
  delay(100);
}
void encenderLed(int numLed) {
  Serial.print("Encender ");
  Serial.println(numLed);
  for (int i=0;i<4;i++) {
    if (i==numLed) {
      digitalWrite(leds[i], HIGH);
    }
    else digitalWrite(leds[i],LOW);
  }
}
long microsecondsToInches(long microseconds) {
  // According to Parallax's datasheet for the PING))), there are 73.746
  // microseconds per inch (i.e. sound travels at 1130 feet per second).
  // This gives the distance travelled by the ping, outbound and return,
  // so we divide by 2 to get the distance of the obstacle.
  // See: http://www.parallax.com/dl/docs/prod/acc/28015-PING-v1.3.pdf
  return microseconds / 74 / 2;
}

long microsecondsToCentimeters(long microseconds) {
  // The speed of sound is 340 m/s or 29 microseconds per centimeter.
  // The ping travels out and back, so to find the distance of the object we
  // take half of the distance travelled.
  return microseconds / 29 / 2;
}
long calculaTiempo(int pingPin, int echoPin) {
  long duration=0;
  pinMode(pingPin, OUTPUT);
  digitalWrite(pingPin, LOW);
  delayMicroseconds(2);
  digitalWrite(pingPin, HIGH);
  delayMicroseconds(5);
  digitalWrite(pingPin, LOW);

  // The same pin is used to read the signal from the PING))): a HIGH pulse
  // whose duration is the time (in microseconds) from the sending of the ping
  // to the reception of its echo off of an object.
  pinMode(echoPin, INPUT);
  duration = pulseIn(echoPin, HIGH);
  return duration;
}
